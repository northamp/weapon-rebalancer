// include sdk headers to communicate with UE3
// WARNING: this header file can currently only be included once!
//   the SDK currently throws alot of warnings which can be ignored
#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <vector>
#include <SdkHeaders.h>


// for access to event manager
#include <Proxy/Events.h>
// for access to client/server
#include <Proxy/Network.h>
// for module api 
#include <Proxy/Modules.h>
// for logging in main file
#include <Proxy/Logger.h>

using namespace BLRevive;

struct RebalancerWeaponStats {
    std::string FriendlyName;
    unsigned long bInfiniteSpareAmmo;
    unsigned long bInstantReload;
    unsigned long bIsDepotWeapon;
    unsigned long bShouldReloadWithInfiniteAmmo;
    unsigned long bCanMelee;
    unsigned long bCanMeleeTeammates;
    unsigned long bQuickThrow;
    unsigned long bCanWieldWithoutAmmo;
    unsigned long bConsumeAmmo;
    unsigned long bSupportsBarrelMods;
    unsigned long bSupportsMagazineMods;
    unsigned long bSupportsMuzzleMods;
    unsigned long bSupportsScopeMods;
    unsigned long bSupportsStockMods;
    unsigned long bSupportsGripMods;
    unsigned long bSupportsCamo;
    unsigned long bSupportsBarrelGripMods;
    unsigned long bSupportsHangerMods;
    unsigned long bSupportsAmmoMods;
    unsigned long bAutomaticWeapon;
    int JumpStaminaAmount;
    float FirstPersonFOV;
    struct FGameBalanceRange DamageRange;
    struct FGameBalanceRange BaseSpreadRange;
    struct FGameBalanceRange TABaseSpreadRange;
    struct FGameBalanceRange SpreadPerShotRange;
    struct FGameBalanceRange SpreadPerShotExpRange;
    struct FGameBalanceRange MaxSpreadRange;
    struct FGameBalanceRange DecayRateRange;
    struct FGameBalanceRange RecoilModifierRange;
    struct FGameBalanceRange MaxDistanceRange;
    struct FGameBalanceRange IdealDistanceRange;
    struct FGameBalanceRange ReloadRateRange;
    struct FGameBalanceRange RecoilReloadRateRange;
    struct FGameBalanceRange MagazineSizeRange;
    struct FGameBalanceRange MaxSpeedRange;
    struct FGameBalanceRange RateOfFireRange;
    struct FGameBalanceRange WeightMultiplerRange;
    struct FGameBalanceRange TightAimTimeRange;
    struct FGameBalanceRange MovementSpeedRange;
    int MovementModifier;
    struct FGameBalanceRange SpreadStatRange;
    struct FGameBalanceRange RecoilStatRange;
    int RateOfFire;
    int BurstFireMax;
    float RechargeTimer;
    int SpareAmmoCount;
    int MaxSpareAmmo;
    float MagSize;
    float MagSizeModifier;
    int InitialMagazines;
    float ReloadDuration;
    float CoopSpareAmmoMultiplier;
    int IdealRange;
    float MaxRange;
    float MaxTraceRange;
    float MaxRangeDamageMultiplier;
    float MeleeRange;
    float MeleeLungeRange;
    float MeleeOuterSearchStepSize;
    float MeleeInnerSearchStepSize;
    float QuickMeleePct;
    float ZoomCurrentFOV;
    float MaxAdhesionTime;
    float MinAdhesionDistance;
    float MinSnapAdhesionDistance;
    float AimAssistScale;
    float AimAssistScaleWhileZoomed;
    //TArray< float > BaseSpread;
    //TArray< float > TABaseSpread;
    float SpreadAccumulationRate;
    float SpreadRecoveryRate;
    float MaximumSpread;
    float SpreadZoomMultiplier;
    float SpreadCrouchMultiplier;
    float SpreadJumpMultiplier;
    float SpreadJumpConstant;
    float MovementSpreadConstant;
    float MovementSpreadMultiplier;
    float SpreadCenter;
    float SpreadCenterWeight;
    float SpreadPerShot;
    float SpreadPerShotExp;
    float SpreadShotCount;
    int SpreadShotCountDecayRate;
    float SpreadShotCountDecayExp;
    float SpreadLastFireTime;
    float ModSpreadMultipler;
    float SpreadAccumulationModifier;
    //TArray< struct FVector2D > SpreadData;
    float MaxOffsetMagnitude;
    float SpreadOffsetNormalWeight;
    float SpreadOffsetTightAimWeight;
    float RecoilSizeModifier;
    float RecoilSize;
    struct FVector2D RecoilSizeVector;
    struct FVector2D PCRecoilReductionMultiplier;
    float PCRecoilAccumulationReductionMultiplier;
    struct FVector2D ConsoleRecoilReductionMultiplier;
    float ConsoleRecoilAccumulationReductionMultiplier;
    //TArray< struct FVector2D > RecoilVectorOffset;
    struct FVector2D MaxRecoilVector;
    struct FVector2D MinRecoilVector;
    float MinRecoilMultipler;
    float RecoilAccumulation;
    float RecoilZoomMultiplier;
    float RecoilRecoveryTime;
    float RecoilApplyTime;
    float WeaponReloadRate;
};

struct RebalancerWeapons {
    std::vector<RebalancerWeaponStats> weapons;
};

static void logError(std::string message) {
    LError("weapon-rebalancer: " + message);
    LFlush;
}

static void logDebug(std::string message) {
    LDebug("weapon-rebalancer: " + message);
    LFlush;
}

static void logWarn(std::string message) {
    LWarn("weapon-rebalancer: " + message);
    LFlush;
}

static RebalancerWeapons rebalancerConfigFromFile();
static void applyStatsToGame(const RebalancerWeapons& rebalancerWeapons);

static RebalancerWeapons rebalancerConfig;

/// <summary>
/// Thread thats specific to the module (function must exist and export demangled!)
/// </summary>
extern "C" __declspec(dllexport) void ModuleThread()
{
    if (!Utils::IsServer()) {
        return;
    }

    rebalancerConfig = rebalancerConfigFromFile();

    std::shared_ptr<Event::Manager> eventManager = Event::Manager::GetInstance();
#if 1
    eventManager->RegisterHandler({
        64192, //Events::ID("*", "IsConfigFiltered"),
        [=](Event::Info info) {
            applyStatsToGame(rebalancerConfig);
            // only run this hook one time
            return true;
        } });
    logDebug("registered oneshot handler for event * IsConfigFiltered");
#endif
}

/// <summary>
/// Module initializer (function must exist and export demangled!)
/// </summary>
/// <param name="data"></param>
extern "C" __declspec(dllexport) void InitializeModule(Module::InitData *data)
{
    // check param validity
    if (!data || !data->EventManager || !data->Logger) {
        LError("module initializer param was null!"); LFlush;
        return;
    }

    // initialize logger (to enable logging to the same file)
    Logger::Link(data->Logger);

    // initialize event manager
    // an instance of the manager can be retrieved with Events::Manager::Instance() afterwards
    Event::Manager::Link(data->EventManager);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

/// <summary>
/// Copy from server-utils' eponymous function
/// </summary>
/// <returns>std::string</returns>
static std::string getConfigPath() {
    static char* pathStr = nullptr;
    if (pathStr == nullptr) {
        std::string path = Utils::FS::BlreviveConfigPath();
        pathStr = new char[path.length() + 1];
        strcpy(pathStr, path.c_str());
        return path;
    }
    else {
        return std::string(pathStr);
    }
}

/// <summary>
/// Copy from server-utils' eponymous function
/// </summary>
/// <returns>std::string</returns>
static std::string getOutputPath() {
    std::string outputPath = getConfigPath() + "weapon_rebalancer/";

    struct stat info;
    if (stat(outputPath.c_str(), &info) == 0) {
        // path exists
        if (info.st_mode & S_IFDIR) {
            // path exists and is a dir
            return outputPath;
        }
        else {
            logError(fmt::format("{0} exists but it is not a directory", outputPath));
            return "";
        }
    }
    else {
        // path do not exist
        if (CreateDirectory(outputPath.c_str(), nullptr)) {
            return outputPath;
        }
        else {
            logError(fmt::format("cannot create directory {0}", outputPath));
            return "";
        }
    }
}

/// <summary>
/// Copy of server-utils' writeConfig function
/// </summary>
/// <param name="toWrite"></param>
/// <param name="path"></param>
static void writeStats(json& toWrite, std::string path) {
    std::ofstream output(path);
    if (!output.is_open()) {
        logError(fmt::format("failed writing weapon stats to {0}", path));
        return;
    }
    output << toWrite.dump(4) << std::endl;
    output.close();
    return;
}

static std::string unrealStringToString(FString& value) {
    if (value.Data == NULL) {
        return std::string("");
    }
    const char* cstr = value.ToChar();
    std::string ret = std::string(cstr);
    free((void*)cstr);
    return ret;
}

static json defaultStatsJson() {
    json defaultStats;

    for (AFoxWeapon* item : UObject::GetInstancesOf<AFoxWeapon>()) {
        if (item->FriendlyName.Data == nullptr) {
            continue;
        }
        std::string friendlyName = unrealStringToString(item->FriendlyName);
        LDebug("Got weapon named {0}", friendlyName);

        defaultStats[friendlyName]["bInfiniteSpareAmmo"] = (bool)item->bInfiniteSpareAmmo;
        defaultStats[friendlyName]["bInstantReload"] = (bool)item->bInstantReload;
        defaultStats[friendlyName]["bIsDepotWeapon"] = (bool)item->bInstantReload;
        defaultStats[friendlyName]["bShouldReloadWithInfiniteAmmo"] = (bool)item->bShouldReloadWithInfiniteAmmo;
        defaultStats[friendlyName]["bCanMelee"] = (bool)item->bCanMelee;
        defaultStats[friendlyName]["bCanMeleeTeammates"] = (bool)item->bCanMeleeTeammates;
        defaultStats[friendlyName]["bQuickThrow"] = (bool)item->bQuickThrow;
        defaultStats[friendlyName]["bCanWieldWithoutAmmo"] = (bool)item->bCanWieldWithoutAmmo;
        defaultStats[friendlyName]["bConsumeAmmo"] = (bool)item->bConsumeAmmo;
        defaultStats[friendlyName]["bSupportsBarrelMods"] = (bool)item->bSupportsBarrelMods;
        defaultStats[friendlyName]["bSupportsMagazineMods"] = (bool)item->bSupportsMagazineMods;
        defaultStats[friendlyName]["bSupportsMuzzleMods"] = (bool)item->bSupportsMuzzleMods;
        defaultStats[friendlyName]["bSupportsScopeMods"] = (bool)item->bSupportsScopeMods;
        defaultStats[friendlyName]["bSupportsStockMods"] = (bool)item->bSupportsStockMods;
        defaultStats[friendlyName]["bSupportsGripMods"] = (bool)item->bSupportsGripMods;
        defaultStats[friendlyName]["bSupportsCamo"] = (bool)item->bSupportsCamo;
        defaultStats[friendlyName]["bSupportsBarrelGripMods"] = (bool)item->bSupportsBarrelGripMods;
        defaultStats[friendlyName]["bSupportsHangerMods"] = (bool)item->bSupportsHangerMods;
        defaultStats[friendlyName]["bSupportsAmmoMods"] = (bool)item->bSupportsAmmoMods;
        defaultStats[friendlyName]["bAutomaticWeapon"] = (bool)item->bAutomaticWeapon;

        defaultStats[friendlyName]["JumpStaminaAmount"] = (int)item->JumpStaminaAmount;
        defaultStats[friendlyName]["FirstPersonFOV"] = (float)item->JumpStaminaAmount;

        defaultStats[friendlyName]["DamageRange"]["Min"] = (float)item->DamageRange.Min;
        defaultStats[friendlyName]["DamageRange"]["Base"] = (float)item->DamageRange.Base;
        defaultStats[friendlyName]["DamageRange"]["Max"] = (float)item->DamageRange.Max;
        
        defaultStats[friendlyName]["BaseSpreadRange"]["Min"] = (float)item->BaseSpreadRange.Min;
        defaultStats[friendlyName]["BaseSpreadRange"]["Base"] = (float)item->BaseSpreadRange.Base;
        defaultStats[friendlyName]["BaseSpreadRange"]["Max"] = (float)item->BaseSpreadRange.Max;

        defaultStats[friendlyName]["TABaseSpreadRange"]["Min"] = (float)item->TABaseSpreadRange.Min;
        defaultStats[friendlyName]["TABaseSpreadRange"]["Base"] = (float)item->TABaseSpreadRange.Base;
        defaultStats[friendlyName]["TABaseSpreadRange"]["Max"] = (float)item->TABaseSpreadRange.Max;

        defaultStats[friendlyName]["SpreadPerShotRange"]["Min"] = (float)item->SpreadPerShotRange.Min;
        defaultStats[friendlyName]["SpreadPerShotRange"]["Base"] = (float)item->SpreadPerShotRange.Base;
        defaultStats[friendlyName]["SpreadPerShotRange"]["Max"] = (float)item->SpreadPerShotRange.Max;

        defaultStats[friendlyName]["SpreadPerShotExpRange"]["Min"] = (float)item->SpreadPerShotExpRange.Min;
        defaultStats[friendlyName]["SpreadPerShotExpRange"]["Base"] = (float)item->SpreadPerShotExpRange.Base;
        defaultStats[friendlyName]["SpreadPerShotExpRange"]["Max"] = (float)item->SpreadPerShotExpRange.Max;

        defaultStats[friendlyName]["MaxSpreadRange"]["Min"] = (float)item->MaxSpreadRange.Min;
        defaultStats[friendlyName]["MaxSpreadRange"]["Base"] = (float)item->MaxSpreadRange.Base;
        defaultStats[friendlyName]["MaxSpreadRange"]["Max"] = (float)item->MaxSpreadRange.Max;

        defaultStats[friendlyName]["DecayRateRange"]["Min"] = (float)item->DecayRateRange.Min;
        defaultStats[friendlyName]["DecayRateRange"]["Base"] = (float)item->DecayRateRange.Base;
        defaultStats[friendlyName]["DecayRateRange"]["Max"] = (float)item->DecayRateRange.Max;

        defaultStats[friendlyName]["RecoilModifierRange"]["Min"] = (float)item->RecoilModifierRange.Min;
        defaultStats[friendlyName]["RecoilModifierRange"]["Base"] = (float)item->RecoilModifierRange.Base;
        defaultStats[friendlyName]["RecoilModifierRange"]["Max"] = (float)item->RecoilModifierRange.Max;

        defaultStats[friendlyName]["MaxDistanceRange"]["Min"] = (float)item->MaxDistanceRange.Min;
        defaultStats[friendlyName]["MaxDistanceRange"]["Base"] = (float)item->MaxDistanceRange.Base;
        defaultStats[friendlyName]["MaxDistanceRange"]["Max"] = (float)item->MaxDistanceRange.Max;

        defaultStats[friendlyName]["IdealDistanceRange"]["Min"] = (float)item->IdealDistanceRange.Min;
        defaultStats[friendlyName]["IdealDistanceRange"]["Base"] = (float)item->IdealDistanceRange.Base;
        defaultStats[friendlyName]["IdealDistanceRange"]["Max"] = (float)item->IdealDistanceRange.Max;

        defaultStats[friendlyName]["ReloadRateRange"]["Min"] = (float)item->ReloadRateRange.Min;
        defaultStats[friendlyName]["ReloadRateRange"]["Base"] = (float)item->ReloadRateRange.Base;
        defaultStats[friendlyName]["ReloadRateRange"]["Max"] = (float)item->ReloadRateRange.Max;

        defaultStats[friendlyName]["RecoilReloadRateRange"]["Min"] = (float)item->RecoilReloadRateRange.Min;
        defaultStats[friendlyName]["RecoilReloadRateRange"]["Base"] = (float)item->RecoilReloadRateRange.Base;
        defaultStats[friendlyName]["RecoilReloadRateRange"]["Max"] = (float)item->RecoilReloadRateRange.Max;

        defaultStats[friendlyName]["MagazineSizeRange"]["Min"] = (float)item->MagazineSizeRange.Min;
        defaultStats[friendlyName]["MagazineSizeRange"]["Base"] = (float)item->MagazineSizeRange.Base;
        defaultStats[friendlyName]["MagazineSizeRange"]["Max"] = (float)item->MagazineSizeRange.Max;

        defaultStats[friendlyName]["MaxSpeedRange"]["Min"] = (float)item->MaxSpeedRange.Min;
        defaultStats[friendlyName]["MaxSpeedRange"]["Base"] = (float)item->MaxSpeedRange.Base;
        defaultStats[friendlyName]["MaxSpeedRange"]["Max"] = (float)item->MaxSpeedRange.Max;

        defaultStats[friendlyName]["RateOfFireRange"]["Min"] = (float)item->RateOfFireRange.Min;
        defaultStats[friendlyName]["RateOfFireRange"]["Base"] = (float)item->RateOfFireRange.Base;
        defaultStats[friendlyName]["RateOfFireRange"]["Max"] = (float)item->RateOfFireRange.Max;

        defaultStats[friendlyName]["WeightMultiplerRange"]["Min"] = (float)item->WeightMultiplerRange.Min;
        defaultStats[friendlyName]["WeightMultiplerRange"]["Base"] = (float)item->WeightMultiplerRange.Base;
        defaultStats[friendlyName]["WeightMultiplerRange"]["Max"] = (float)item->WeightMultiplerRange.Max;

        defaultStats[friendlyName]["TightAimTimeRange"]["Min"] = (float)item->TightAimTimeRange.Min;
        defaultStats[friendlyName]["TightAimTimeRange"]["Base"] = (float)item->TightAimTimeRange.Base;
        defaultStats[friendlyName]["TightAimTimeRange"]["Max"] = (float)item->TightAimTimeRange.Max;

        defaultStats[friendlyName]["MovementSpeedRange"]["Min"] = (float)item->TightAimTimeRange.Min;
        defaultStats[friendlyName]["MovementSpeedRange"]["Base"] = (float)item->TightAimTimeRange.Base;
        defaultStats[friendlyName]["MovementSpeedRange"]["Max"] = (float)item->TightAimTimeRange.Max;

        defaultStats[friendlyName]["MovementModifier"] = (int)item->MovementModifier;

        defaultStats[friendlyName]["SpreadStatRange"]["Min"] = (float)item->SpreadStatRange.Min;
        defaultStats[friendlyName]["SpreadStatRange"]["Base"] = (float)item->SpreadStatRange.Base;
        defaultStats[friendlyName]["SpreadStatRange"]["Max"] = (float)item->SpreadStatRange.Max;

        defaultStats[friendlyName]["RecoilStatRange"]["Min"] = (float)item->RecoilStatRange.Min;
        defaultStats[friendlyName]["RecoilStatRange"]["Base"] = (float)item->RecoilStatRange.Base;
        defaultStats[friendlyName]["RecoilStatRange"]["Max"] = (float)item->RecoilStatRange.Max;

        defaultStats[friendlyName]["RateOfFire"] = (int)item->RateOfFire;
        defaultStats[friendlyName]["BurstFireMax"] = (int)item->BurstFireMax;
        defaultStats[friendlyName]["RechargeTimer"] = (float)item->RechargeTimer;
        defaultStats[friendlyName]["SpareAmmoCount"] = (int)item->SpareAmmoCount;
        defaultStats[friendlyName]["MaxSpareAmmo"] = (int)item->MaxSpareAmmo;
        defaultStats[friendlyName]["MagSize"] = (float)item->MagSize;
        defaultStats[friendlyName]["MagSizeModifier"] = (float)item->MagSizeModifier;
        defaultStats[friendlyName]["InitialMagazines"] = (int)item->InitialMagazines;
        defaultStats[friendlyName]["ReloadDuration"] = (float)item->ReloadDuration;
        defaultStats[friendlyName]["CoopSpareAmmoMultiplier"] = (float)item->CoopSpareAmmoMultiplier;
        defaultStats[friendlyName]["IdealRange"] = (int)item->IdealRange;
        defaultStats[friendlyName]["MaxRange"] = (float)item->MaxRange;
        defaultStats[friendlyName]["MaxTraceRange"] = (float)item->MaxTraceRange;
        defaultStats[friendlyName]["MaxRangeDamageMultiplier"] = (float)item->MaxRangeDamageMultiplier;
        defaultStats[friendlyName]["MeleeRange"] = (float)item->MeleeRange;
        defaultStats[friendlyName]["MeleeLungeRange"] = (float)item->MeleeLungeRange;
        defaultStats[friendlyName]["MeleeOuterSearchStepSize"] = (float)item->MeleeOuterSearchStepSize;
        defaultStats[friendlyName]["MeleeInnerSearchStepSize"] = (float)item->MeleeInnerSearchStepSize;
        defaultStats[friendlyName]["QuickMeleePct"] = (float)item->QuickMeleePct;
        defaultStats[friendlyName]["ZoomCurrentFOV"] = (float)item->ZoomCurrentFOV;
        defaultStats[friendlyName]["MaxAdhesionTime"] = (float)item->MaxAdhesionTime;
        defaultStats[friendlyName]["MinAdhesionDistance"] = (float)item->MinAdhesionDistance;
        defaultStats[friendlyName]["MinSnapAdhesionDistance"] = (float)item->MinSnapAdhesionDistance;
        defaultStats[friendlyName]["AimAssistScale"] = (float)item->AimAssistScale;
        defaultStats[friendlyName]["AimAssistScaleWhileZoomed"] = (float)item->AimAssistScaleWhileZoomed;

        // Later
        //TArray< float > BaseSpread;
        //TArray< float > TABaseSpread;
        
        defaultStats[friendlyName]["SpreadAccumulationRate"] = (float)item->SpreadAccumulationRate;
        defaultStats[friendlyName]["SpreadRecoveryRate"] = (float)item->SpreadRecoveryRate;
        defaultStats[friendlyName]["MaximumSpread"] = (float)item->MaximumSpread;
        defaultStats[friendlyName]["SpreadZoomMultiplier"] = (float)item->SpreadZoomMultiplier;
        defaultStats[friendlyName]["SpreadCrouchMultiplier"] = (float)item->SpreadCrouchMultiplier;
        defaultStats[friendlyName]["SpreadJumpMultiplier"] = (float)item->SpreadJumpMultiplier;
        defaultStats[friendlyName]["SpreadJumpConstant"] = (float)item->SpreadJumpConstant;
        defaultStats[friendlyName]["MovementSpreadConstant"] = (float)item->MovementSpreadConstant;
        defaultStats[friendlyName]["MovementSpreadMultiplier"] = (float)item->MovementSpreadMultiplier;
        defaultStats[friendlyName]["SpreadCenter"] = (float)item->SpreadCenter;
        defaultStats[friendlyName]["SpreadCenterWeight"] = (float)item->SpreadCenterWeight;
        defaultStats[friendlyName]["SpreadPerShot"] = (float)item->SpreadPerShot;
        defaultStats[friendlyName]["SpreadPerShotExp"] = (float)item->SpreadPerShotExp;
        defaultStats[friendlyName]["SpreadShotCount"] = (float)item->SpreadShotCount;
        defaultStats[friendlyName]["SpreadShotCountDecayRate"] = (int)item->SpreadShotCountDecayRate;
        defaultStats[friendlyName]["SpreadShotCountDecayExp"] = (float)item->SpreadShotCountDecayExp;
        defaultStats[friendlyName]["SpreadLastFireTime"] = (float)item->SpreadLastFireTime;
        defaultStats[friendlyName]["ModSpreadMultipler"] = (float)item->ModSpreadMultipler;
        defaultStats[friendlyName]["SpreadAccumulationModifier"] = (float)item->SpreadAccumulationModifier;

        // Later
        //TArray< struct FVector2D > SpreadData;
        
        defaultStats[friendlyName]["MaxOffsetMagnitude"] = (float)item->MaxOffsetMagnitude;
        defaultStats[friendlyName]["SpreadOffsetNormalWeight"] = (float)item->SpreadOffsetNormalWeight;
        defaultStats[friendlyName]["SpreadOffsetTightAimWeight"] = (float)item->SpreadOffsetTightAimWeight;
        defaultStats[friendlyName]["RecoilSizeModifier"] = (float)item->RecoilSizeModifier;
        defaultStats[friendlyName]["RecoilSize"] = (float)item->RecoilSize;

        defaultStats[friendlyName]["RecoilSizeVector"]["X"] = (float)item->RecoilSizeVector.X;
        defaultStats[friendlyName]["RecoilSizeVector"]["Y"] = (float)item->RecoilSizeVector.Y;

        defaultStats[friendlyName]["PCRecoilReductionMultiplier"]["X"] = (float)item->PCRecoilReductionMultiplier.X;
        defaultStats[friendlyName]["PCRecoilReductionMultiplier"]["Y"] = (float)item->PCRecoilReductionMultiplier.Y;

        defaultStats[friendlyName]["PCRecoilAccumulationReductionMultiplier"] = (float)item->PCRecoilAccumulationReductionMultiplier;
        
        defaultStats[friendlyName]["ConsoleRecoilReductionMultiplier"]["X"] = (float)item->ConsoleRecoilReductionMultiplier.X;
        defaultStats[friendlyName]["ConsoleRecoilReductionMultiplier"]["Y"] = (float)item->ConsoleRecoilReductionMultiplier.Y;

        defaultStats[friendlyName]["ConsoleRecoilAccumulationReductionMultiplier"] = (float)item->ConsoleRecoilAccumulationReductionMultiplier;

        // Later
        //TArray< struct FVector2D > RecoilVectorOffset;
        
        defaultStats[friendlyName]["MaxRecoilVector"]["X"] = (float)item->MaxRecoilVector.X;
        defaultStats[friendlyName]["MaxRecoilVector"]["Y"] = (float)item->MaxRecoilVector.Y;

        defaultStats[friendlyName]["MinRecoilVector"]["X"] = (float)item->MinRecoilVector.X;
        defaultStats[friendlyName]["MinRecoilVector"]["Y"] = (float)item->MinRecoilVector.Y;

        defaultStats[friendlyName]["MinRecoilMultipler"] = (float)item->MinRecoilMultipler;
        defaultStats[friendlyName]["RecoilAccumulation"] = (float)item->RecoilAccumulation;
        defaultStats[friendlyName]["RecoilZoomMultiplier"] = (float)item->RecoilZoomMultiplier;
        defaultStats[friendlyName]["RecoilRecoveryTime"] = (float)item->RecoilRecoveryTime;
        defaultStats[friendlyName]["RecoilApplyTime"] = (float)item->RecoilApplyTime;
        defaultStats[friendlyName]["WeaponReloadRate"] = (float)item->WeaponReloadRate;
    }
    return defaultStats;
}

/// <summary>
/// Mildly butchered copy of server-util's eponymous function
/// </summary>
/// <param name="input"></param>
/// <param name="defaultOverlay"></param>
/// <param name="weapon"></param>
/// <param name="param1"></param>
/// <param name="param2"></param>
/// <returns></returns>
static json getJsonValue(json& input, json& defaultOverlay, std::string weapon, std::string param1, std::string param2 = "") {
    json val;
    if (param2 == "") {
        try {
            val = input[weapon][param1];
            if (val.is_null()) {
                logWarn(fmt::format("{0}/{1} not found in config, using default value", weapon, param1));
                val = defaultOverlay[param1];
            }
        }
        catch (json::exception e) {
            throw(e);
        }
        return val;
    }
    else {
        try {
            val = input[weapon][param1][param2];
            if (val.is_null()) {
                logWarn(fmt::format("{0}/{1}/{2} not found in config, using default value", weapon, param1, param2));
                val = defaultOverlay[param1][param2];
            }
        }
        catch (json::exception e) {
            throw(e);
        }
        return val;
    }
}

static RebalancerWeapons rebalancerConfigFromJson(json input) {
    RebalancerWeapons config;
    json defaultStats = defaultStatsJson();
    json val;
    try {
        for (const auto& item : defaultStats.items()) {
            RebalancerWeaponStats newWeapon;
            newWeapon.FriendlyName = item.key();

            newWeapon.bInfiniteSpareAmmo = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bInfiniteSpareAmmo");
            newWeapon.bInstantReload = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bInstantReload");
            newWeapon.bIsDepotWeapon = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bIsDepotWeapon");
            newWeapon.bShouldReloadWithInfiniteAmmo = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bShouldReloadWithInfiniteAmmo");
            newWeapon.bCanMelee = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bCanMelee");
            newWeapon.bCanMeleeTeammates = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bCanMeleeTeammates");
            newWeapon.bQuickThrow = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bQuickThrow");
            newWeapon.bCanWieldWithoutAmmo = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bCanWieldWithoutAmmo");
            newWeapon.bConsumeAmmo = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bConsumeAmmo");
            newWeapon.bSupportsBarrelMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsBarrelMods");
            newWeapon.bSupportsMagazineMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsMagazineMods");
            newWeapon.bSupportsMuzzleMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsMuzzleMods");
            newWeapon.bSupportsScopeMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsScopeMods");
            newWeapon.bSupportsStockMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsStockMods");
            newWeapon.bSupportsGripMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsGripMods");
            newWeapon.bSupportsCamo = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsCamo");
            newWeapon.bSupportsBarrelGripMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsBarrelGripMods");
            newWeapon.bSupportsHangerMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsHangerMods");
            newWeapon.bSupportsAmmoMods = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bSupportsAmmoMods");
            newWeapon.bAutomaticWeapon = (unsigned long)getJsonValue(input, defaultStats, item.key(), "bAutomaticWeapon");

            newWeapon.JumpStaminaAmount = (int)getJsonValue(input, defaultStats, item.key(), "JumpStaminaAmount");
            newWeapon.FirstPersonFOV = (float)getJsonValue(input, defaultStats, item.key(), "FirstPersonFOV");

            newWeapon.DamageRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "DamageRange", "Min");
            newWeapon.DamageRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "DamageRange", "Base");
            newWeapon.DamageRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "DamageRange", "Max");

            newWeapon.BaseSpreadRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "BaseSpreadRange", "Min");
            newWeapon.BaseSpreadRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "BaseSpreadRange", "Base");
            newWeapon.BaseSpreadRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "BaseSpreadRange", "Max");

            newWeapon.TABaseSpreadRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "TABaseSpreadRange", "Min");
            newWeapon.TABaseSpreadRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "TABaseSpreadRange", "Base");
            newWeapon.TABaseSpreadRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "TABaseSpreadRange", "Max");

            newWeapon.SpreadPerShotRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotRange", "Min");
            newWeapon.SpreadPerShotRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotRange", "Base");
            newWeapon.SpreadPerShotRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotRange", "Max");

            newWeapon.SpreadPerShotExpRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotExpRange", "Min");
            newWeapon.SpreadPerShotExpRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotExpRange", "Base");
            newWeapon.SpreadPerShotExpRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotExpRange", "Max");

            newWeapon.MaxSpreadRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "MaxSpreadRange", "Min");
            newWeapon.MaxSpreadRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "MaxSpreadRange", "Base");
            newWeapon.MaxSpreadRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "MaxSpreadRange", "Max");

            newWeapon.DecayRateRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "DecayRateRange", "Min");
            newWeapon.DecayRateRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "DecayRateRange", "Base");
            newWeapon.DecayRateRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "DecayRateRange", "Max");

            newWeapon.RecoilModifierRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "RecoilModifierRange", "Min");
            newWeapon.RecoilModifierRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "RecoilModifierRange", "Base");
            newWeapon.RecoilModifierRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "RecoilModifierRange", "Max");

            newWeapon.MaxDistanceRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "MaxDistanceRange", "Min");
            newWeapon.MaxDistanceRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "MaxDistanceRange", "Base");
            newWeapon.MaxDistanceRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "MaxDistanceRange", "Max");

            newWeapon.IdealDistanceRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "IdealDistanceRange", "Min");
            newWeapon.IdealDistanceRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "IdealDistanceRange", "Base");
            newWeapon.IdealDistanceRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "IdealDistanceRange", "Max");

            newWeapon.ReloadRateRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "ReloadRateRange", "Min");
            newWeapon.ReloadRateRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "ReloadRateRange", "Base");
            newWeapon.ReloadRateRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "ReloadRateRange", "Max");

            newWeapon.RecoilReloadRateRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "RecoilReloadRateRange", "Min");
            newWeapon.RecoilReloadRateRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "RecoilReloadRateRange", "Base");
            newWeapon.RecoilReloadRateRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "RecoilReloadRateRange", "Max");

            newWeapon.MagazineSizeRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "MagazineSizeRange", "Min");
            newWeapon.MagazineSizeRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "MagazineSizeRange", "Base");
            newWeapon.MagazineSizeRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "MagazineSizeRange", "Max");

            newWeapon.MaxSpeedRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "MaxSpeedRange", "Min");
            newWeapon.MaxSpeedRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "MaxSpeedRange", "Base");
            newWeapon.MaxSpeedRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "MaxSpeedRange", "Max");

            newWeapon.RateOfFireRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "RateOfFireRange", "Min");
            newWeapon.RateOfFireRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "RateOfFireRange", "Base");
            newWeapon.RateOfFireRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "RateOfFireRange", "Max");

            newWeapon.WeightMultiplerRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "WeightMultiplerRange", "Min");
            newWeapon.WeightMultiplerRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "WeightMultiplerRange", "Base");
            newWeapon.WeightMultiplerRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "WeightMultiplerRange", "Max");

            newWeapon.TightAimTimeRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "TightAimTimeRange", "Min");
            newWeapon.TightAimTimeRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "TightAimTimeRange", "Base");
            newWeapon.TightAimTimeRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "TightAimTimeRange", "Max");

            newWeapon.MovementSpeedRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "MovementSpeedRange", "Min");
            newWeapon.MovementSpeedRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "MovementSpeedRange", "Base");
            newWeapon.MovementSpeedRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "MovementSpeedRange", "Max");

            newWeapon.MovementModifier = (unsigned long)getJsonValue(input, defaultStats, item.key(), "MovementModifier");

            newWeapon.SpreadStatRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "SpreadStatRange", "Min");
            newWeapon.SpreadStatRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "SpreadStatRange", "Base");
            newWeapon.SpreadStatRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "SpreadStatRange", "Max");

            newWeapon.RecoilStatRange.Min = (float)getJsonValue(input, defaultStats, item.key(), "RecoilStatRange", "Min");
            newWeapon.RecoilStatRange.Base = (float)getJsonValue(input, defaultStats, item.key(), "RecoilStatRange", "Base");
            newWeapon.RecoilStatRange.Max = (float)getJsonValue(input, defaultStats, item.key(), "RecoilStatRange", "Max");

            newWeapon.RateOfFire = (int)getJsonValue(input, defaultStats, item.key(), "RateOfFire");
            newWeapon.BurstFireMax = (int)getJsonValue(input, defaultStats, item.key(), "BurstFireMax");
            newWeapon.RechargeTimer = (float)getJsonValue(input, defaultStats, item.key(), "RechargeTimer");
            newWeapon.SpareAmmoCount = (int)getJsonValue(input, defaultStats, item.key(), "SpareAmmoCount");
            newWeapon.MaxSpareAmmo = (int)getJsonValue(input, defaultStats, item.key(), "MaxSpareAmmo");
            newWeapon.MagSize = (float)getJsonValue(input, defaultStats, item.key(), "MagSize");
            newWeapon.MagSizeModifier = (float)getJsonValue(input, defaultStats, item.key(), "MagSizeModifier");
            newWeapon.InitialMagazines = (int)getJsonValue(input, defaultStats, item.key(), "InitialMagazines");
            newWeapon.ReloadDuration = (float)getJsonValue(input, defaultStats, item.key(), "ReloadDuration");
            newWeapon.CoopSpareAmmoMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "CoopSpareAmmoMultiplier");
            newWeapon.IdealRange = (int)getJsonValue(input, defaultStats, item.key(), "IdealRange");
            newWeapon.MaxRange = (float)getJsonValue(input, defaultStats, item.key(), "MaxRange");
            newWeapon.MaxTraceRange = (float)getJsonValue(input, defaultStats, item.key(), "MaxTraceRange");
            newWeapon.MaxRangeDamageMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "MaxRangeDamageMultiplier");
            newWeapon.MeleeRange = (float)getJsonValue(input, defaultStats, item.key(), "MeleeRange");
            newWeapon.MeleeLungeRange = (float)getJsonValue(input, defaultStats, item.key(), "MeleeLungeRange");
            newWeapon.MeleeOuterSearchStepSize = (float)getJsonValue(input, defaultStats, item.key(), "MeleeOuterSearchStepSize");
            newWeapon.MeleeInnerSearchStepSize = (float)getJsonValue(input, defaultStats, item.key(), "MeleeInnerSearchStepSize");
            newWeapon.QuickMeleePct = (float)getJsonValue(input, defaultStats, item.key(), "QuickMeleePct");
            newWeapon.ZoomCurrentFOV = (float)getJsonValue(input, defaultStats, item.key(), "ZoomCurrentFOV");
            newWeapon.MaxAdhesionTime = (float)getJsonValue(input, defaultStats, item.key(), "MaxAdhesionTime");
            newWeapon.MinAdhesionDistance = (float)getJsonValue(input, defaultStats, item.key(), "MinAdhesionDistance");
            newWeapon.MinSnapAdhesionDistance = (float)getJsonValue(input, defaultStats, item.key(), "MinSnapAdhesionDistance");
            newWeapon.AimAssistScale = (float)getJsonValue(input, defaultStats, item.key(), "AimAssistScale");
            newWeapon.AimAssistScaleWhileZoomed = (float)getJsonValue(input, defaultStats, item.key(), "AimAssistScaleWhileZoomed");

            // Later
            //TArray< float > BaseSpread;
            //TArray< float > TABaseSpread;


            newWeapon.SpreadAccumulationRate = (float)getJsonValue(input, defaultStats, item.key(), "SpreadAccumulationRate");
            newWeapon.SpreadRecoveryRate = (float)getJsonValue(input, defaultStats, item.key(), "SpreadRecoveryRate");
            newWeapon.MaximumSpread = (float)getJsonValue(input, defaultStats, item.key(), "MaximumSpread");
            newWeapon.SpreadZoomMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "SpreadZoomMultiplier");
            newWeapon.SpreadCrouchMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "SpreadCrouchMultiplier");
            newWeapon.SpreadJumpMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "SpreadJumpMultiplier");
            newWeapon.SpreadJumpConstant = (float)getJsonValue(input, defaultStats, item.key(), "SpreadJumpConstant");
            newWeapon.MovementSpreadConstant = (float)getJsonValue(input, defaultStats, item.key(), "MovementSpreadConstant");
            newWeapon.MovementSpreadMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "MovementSpreadMultiplier");
            newWeapon.SpreadCenter = (float)getJsonValue(input, defaultStats, item.key(), "SpreadCenter");
            newWeapon.SpreadCenterWeight = (float)getJsonValue(input, defaultStats, item.key(), "SpreadCenterWeight");
            newWeapon.SpreadPerShot = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShot");
            newWeapon.SpreadPerShotExp = (float)getJsonValue(input, defaultStats, item.key(), "SpreadPerShotExp");
            newWeapon.SpreadShotCount = (float)getJsonValue(input, defaultStats, item.key(), "SpreadShotCount");
            newWeapon.SpreadShotCountDecayRate = (int)getJsonValue(input, defaultStats, item.key(), "SpreadShotCountDecayRate");
            newWeapon.SpreadShotCountDecayExp = (float)getJsonValue(input, defaultStats, item.key(), "SpreadShotCountDecayExp");
            newWeapon.SpreadLastFireTime = (float)getJsonValue(input, defaultStats, item.key(), "SpreadLastFireTime");
            newWeapon.ModSpreadMultipler = (float)getJsonValue(input, defaultStats, item.key(), "ModSpreadMultipler");
            newWeapon.SpreadAccumulationModifier = (float)getJsonValue(input, defaultStats, item.key(), "SpreadAccumulationModifier");

            // Later
            //TArray< struct FVector2D > SpreadData;

            newWeapon.MaxOffsetMagnitude = (float)getJsonValue(input, defaultStats, item.key(), "MaxOffsetMagnitude");
            newWeapon.SpreadOffsetNormalWeight = (float)getJsonValue(input, defaultStats, item.key(), "SpreadOffsetNormalWeight");
            newWeapon.SpreadOffsetTightAimWeight = (float)getJsonValue(input, defaultStats, item.key(), "SpreadOffsetTightAimWeight");
            newWeapon.RecoilSizeModifier = (float)getJsonValue(input, defaultStats, item.key(), "RecoilSizeModifier");
            newWeapon.RecoilSize = (float)getJsonValue(input, defaultStats, item.key(), "RecoilSize");

            newWeapon.RecoilSizeVector.X = (float)getJsonValue(input, defaultStats, item.key(), "RecoilSizeVector", "X");
            newWeapon.RecoilSizeVector.Y = (float)getJsonValue(input, defaultStats, item.key(), "RecoilSizeVector", "Y");

            newWeapon.PCRecoilReductionMultiplier.X = (float)getJsonValue(input, defaultStats, item.key(), "PCRecoilReductionMultiplier", "X");
            newWeapon.PCRecoilReductionMultiplier.Y = (float)getJsonValue(input, defaultStats, item.key(), "PCRecoilReductionMultiplier", "Y");

            newWeapon.PCRecoilAccumulationReductionMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "PCRecoilAccumulationReductionMultiplier");

            newWeapon.ConsoleRecoilReductionMultiplier.X = (float)getJsonValue(input, defaultStats, item.key(), "ConsoleRecoilReductionMultiplier", "X");
            newWeapon.ConsoleRecoilReductionMultiplier.Y = (float)getJsonValue(input, defaultStats, item.key(), "ConsoleRecoilReductionMultiplier", "Y");

            newWeapon.ConsoleRecoilAccumulationReductionMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "ConsoleRecoilAccumulationReductionMultiplier");

            // Later
            //TArray< struct FVector2D > RecoilVectorOffset;

            newWeapon.MaxRecoilVector.X = (float)getJsonValue(input, defaultStats, item.key(), "MaxRecoilVector", "X");
            newWeapon.MaxRecoilVector.Y = (float)getJsonValue(input, defaultStats, item.key(), "MaxRecoilVector", "Y");

            newWeapon.MinRecoilVector.X = (float)getJsonValue(input, defaultStats, item.key(), "MinRecoilVector", "X");
            newWeapon.MinRecoilVector.Y = (float)getJsonValue(input, defaultStats, item.key(), "MinRecoilVector", "Y");

            newWeapon.MinRecoilMultipler = (float)getJsonValue(input, defaultStats, item.key(), "MinRecoilMultipler");
            newWeapon.RecoilAccumulation = (float)getJsonValue(input, defaultStats, item.key(), "RecoilAccumulation");
            newWeapon.RecoilZoomMultiplier = (float)getJsonValue(input, defaultStats, item.key(), "RecoilZoomMultiplier");
            newWeapon.RecoilRecoveryTime = (float)getJsonValue(input, defaultStats, item.key(), "RecoilRecoveryTime");
            newWeapon.RecoilApplyTime = (float)getJsonValue(input, defaultStats, item.key(), "RecoilApplyTime");
            newWeapon.WeaponReloadRate = (float)getJsonValue(input, defaultStats, item.key(), "WeaponReloadRate");

            config.weapons.push_back(newWeapon);
        }
    }
    catch (json::exception e) {
        throw(e);
    }
    return config;
}

static RebalancerWeapons rebalancerConfigFromFile() {
    std::string outputPath = getOutputPath();
    if (outputPath.length() == 0) {
        logError(fmt::format("cannot load config from {0}", outputPath));
        return rebalancerConfigFromJson(defaultStatsJson());
    }

    std::string weaponStatsPath = fmt::format("{0}{1}", outputPath, "weapon_stats.json");
    std::ifstream input(weaponStatsPath);
    if (!input.is_open()) {
        logDebug(fmt::format("{0} does not exist, writing default config", weaponStatsPath));
        json stats = defaultStatsJson();
        writeStats(stats, weaponStatsPath);
        return rebalancerConfigFromJson(stats);
    }

    try {
        json inputJson = json::parse(input);
        input.close();
        return rebalancerConfigFromJson(inputJson);
    }
    catch (json::exception e) {
        logError(fmt::format("failed parsing {0}, disregarding config", weaponStatsPath));
        logError(e.what());
        input.close();
        return rebalancerConfigFromJson(defaultStatsJson());
    }
}

static void applyStatsToGame(const RebalancerWeapons& rebalancerWeapons) {
    for (RebalancerWeaponStats rebalancerWeapon : rebalancerWeapons.weapons)
    {
        AFoxWeapon* weapon;
        int unlockID = -1;
        for (AFoxWeapon* item : UObject::GetInstancesOf<AFoxWeapon>()) {
            if (item->FriendlyName.Data == nullptr) {
                continue;
            }
            if (rebalancerWeapon.FriendlyName.compare(unrealStringToString(item->FriendlyName)) == 0) {
                weapon = item;
                break;
            }
        }
        logDebug(fmt::format("Found the appropriate object, attempting to patch weapon {0}", rebalancerWeapon.FriendlyName));

        weapon->bInfiniteSpareAmmo = rebalancerWeapon.bInfiniteSpareAmmo;

        weapon->bInstantReload = rebalancerWeapon.bInstantReload;
        weapon->bIsDepotWeapon = rebalancerWeapon.bIsDepotWeapon;
        weapon->bShouldReloadWithInfiniteAmmo = rebalancerWeapon.bShouldReloadWithInfiniteAmmo;
        weapon->bCanMelee = rebalancerWeapon.bCanMelee;
        weapon->bCanMeleeTeammates = rebalancerWeapon.bCanMeleeTeammates;
        weapon->bQuickThrow = rebalancerWeapon.bQuickThrow;
        weapon->bCanWieldWithoutAmmo = rebalancerWeapon.bCanWieldWithoutAmmo;
        weapon->bConsumeAmmo = rebalancerWeapon.bConsumeAmmo;
        weapon->bSupportsBarrelMods = rebalancerWeapon.bSupportsBarrelMods;
        weapon->bSupportsMagazineMods = rebalancerWeapon.bSupportsMagazineMods;
        weapon->bSupportsMuzzleMods = rebalancerWeapon.bSupportsMuzzleMods;
        weapon->bSupportsScopeMods = rebalancerWeapon.bSupportsScopeMods;
        weapon->bSupportsStockMods = rebalancerWeapon.bSupportsStockMods;
        weapon->bSupportsGripMods = rebalancerWeapon.bSupportsGripMods;
        weapon->bSupportsCamo = rebalancerWeapon.bSupportsCamo;
        weapon->bSupportsBarrelGripMods = rebalancerWeapon.bSupportsBarrelGripMods;
        weapon->bSupportsHangerMods = rebalancerWeapon.bSupportsHangerMods;
        weapon->bSupportsAmmoMods = rebalancerWeapon.bSupportsAmmoMods;
        weapon->bAutomaticWeapon = rebalancerWeapon.bAutomaticWeapon;

        weapon->JumpStaminaAmount = rebalancerWeapon.JumpStaminaAmount;
        weapon->FirstPersonFOV = rebalancerWeapon.FirstPersonFOV;

        weapon->DamageRange.Min = rebalancerWeapon.DamageRange.Min;
        weapon->DamageRange.Base = rebalancerWeapon.DamageRange.Base;
        weapon->DamageRange.Max = rebalancerWeapon.DamageRange.Max;

        weapon->BaseSpreadRange.Min = rebalancerWeapon.BaseSpreadRange.Min;
        weapon->BaseSpreadRange.Base = rebalancerWeapon.BaseSpreadRange.Base;
        weapon->BaseSpreadRange.Max = rebalancerWeapon.BaseSpreadRange.Max;

        weapon->TABaseSpreadRange.Min = rebalancerWeapon.TABaseSpreadRange.Min;
        weapon->TABaseSpreadRange.Base = rebalancerWeapon.TABaseSpreadRange.Base;
        weapon->TABaseSpreadRange.Max = rebalancerWeapon.TABaseSpreadRange.Max;

        weapon->SpreadPerShotRange.Min = rebalancerWeapon.SpreadPerShotRange.Min;
        weapon->SpreadPerShotRange.Base = rebalancerWeapon.SpreadPerShotRange.Base;
        weapon->SpreadPerShotRange.Max = rebalancerWeapon.SpreadPerShotRange.Max;

        weapon->SpreadPerShotExpRange.Min = rebalancerWeapon.SpreadPerShotExpRange.Min;
        weapon->SpreadPerShotExpRange.Base = rebalancerWeapon.SpreadPerShotExpRange.Base;
        weapon->SpreadPerShotExpRange.Max = rebalancerWeapon.SpreadPerShotExpRange.Max;

        weapon->MaxSpreadRange.Min = rebalancerWeapon.MaxSpreadRange.Min;
        weapon->MaxSpreadRange.Base = rebalancerWeapon.MaxSpreadRange.Base;
        weapon->MaxSpreadRange.Max = rebalancerWeapon.MaxSpreadRange.Max;

        weapon->DecayRateRange.Min = rebalancerWeapon.DecayRateRange.Min;
        weapon->DecayRateRange.Base = rebalancerWeapon.DecayRateRange.Base;
        weapon->DecayRateRange.Max = rebalancerWeapon.DecayRateRange.Max;

        weapon->RecoilModifierRange.Min = rebalancerWeapon.RecoilModifierRange.Min;
        weapon->RecoilModifierRange.Base = rebalancerWeapon.RecoilModifierRange.Base;
        weapon->RecoilModifierRange.Max = rebalancerWeapon.RecoilModifierRange.Max;

        weapon->MaxDistanceRange.Min = rebalancerWeapon.MaxDistanceRange.Min;
        weapon->MaxDistanceRange.Base = rebalancerWeapon.MaxDistanceRange.Base;
        weapon->MaxDistanceRange.Max = rebalancerWeapon.MaxDistanceRange.Max;

        weapon->IdealDistanceRange.Min = rebalancerWeapon.IdealDistanceRange.Min;
        weapon->IdealDistanceRange.Base = rebalancerWeapon.IdealDistanceRange.Base;
        weapon->IdealDistanceRange.Max = rebalancerWeapon.IdealDistanceRange.Max;

        weapon->ReloadRateRange.Min = rebalancerWeapon.ReloadRateRange.Min;
        weapon->ReloadRateRange.Base = rebalancerWeapon.ReloadRateRange.Base;
        weapon->ReloadRateRange.Max = rebalancerWeapon.ReloadRateRange.Max;

        weapon->RecoilReloadRateRange.Min = rebalancerWeapon.RecoilReloadRateRange.Min;
        weapon->RecoilReloadRateRange.Base = rebalancerWeapon.RecoilReloadRateRange.Base;
        weapon->RecoilReloadRateRange.Max = rebalancerWeapon.RecoilReloadRateRange.Max;

        weapon->MagazineSizeRange.Min = rebalancerWeapon.MagazineSizeRange.Min;
        weapon->MagazineSizeRange.Base = rebalancerWeapon.MagazineSizeRange.Base;
        weapon->MagazineSizeRange.Max = rebalancerWeapon.MagazineSizeRange.Max;

        weapon->MaxSpeedRange.Min = rebalancerWeapon.MaxSpeedRange.Min;
        weapon->MaxSpeedRange.Base = rebalancerWeapon.MaxSpeedRange.Base;
        weapon->MaxSpeedRange.Max = rebalancerWeapon.MaxSpeedRange.Max;

        weapon->RateOfFireRange.Min = rebalancerWeapon.RateOfFireRange.Min;
        weapon->RateOfFireRange.Base = rebalancerWeapon.RateOfFireRange.Base;
        weapon->RateOfFireRange.Max = rebalancerWeapon.RateOfFireRange.Max;

        weapon->WeightMultiplerRange.Min = rebalancerWeapon.WeightMultiplerRange.Min;
        weapon->WeightMultiplerRange.Base = rebalancerWeapon.WeightMultiplerRange.Base;
        weapon->WeightMultiplerRange.Max = rebalancerWeapon.WeightMultiplerRange.Max;

        weapon->TightAimTimeRange.Min = rebalancerWeapon.TightAimTimeRange.Min;
        weapon->TightAimTimeRange.Base = rebalancerWeapon.TightAimTimeRange.Base;
        weapon->TightAimTimeRange.Max = rebalancerWeapon.TightAimTimeRange.Max;

        weapon->MovementSpeedRange.Min = rebalancerWeapon.MovementSpeedRange.Min;
        weapon->MovementSpeedRange.Base = rebalancerWeapon.MovementSpeedRange.Base;
        weapon->MovementSpeedRange.Max = rebalancerWeapon.MovementSpeedRange.Max;

        weapon->MovementModifier = rebalancerWeapon.MovementModifier;

        weapon->SpreadStatRange.Min = rebalancerWeapon.SpreadStatRange.Min;
        weapon->SpreadStatRange.Base = rebalancerWeapon.SpreadStatRange.Base;
        weapon->SpreadStatRange.Max = rebalancerWeapon.SpreadStatRange.Max;

        weapon->RecoilStatRange.Min = rebalancerWeapon.RecoilStatRange.Min;
        weapon->RecoilStatRange.Base = rebalancerWeapon.RecoilStatRange.Base;
        weapon->RecoilStatRange.Max = rebalancerWeapon.RecoilStatRange.Max;

        weapon->RateOfFire = rebalancerWeapon.RateOfFire;
        weapon->BurstFireMax = rebalancerWeapon.BurstFireMax;
        weapon->RechargeTimer = rebalancerWeapon.RechargeTimer;
        weapon->SpareAmmoCount = rebalancerWeapon.SpareAmmoCount;
        weapon->MaxSpareAmmo = rebalancerWeapon.MaxSpareAmmo;
        weapon->MagSize = rebalancerWeapon.MagSize;
        weapon->MagSizeModifier = rebalancerWeapon.MagSizeModifier;
        weapon->InitialMagazines = rebalancerWeapon.InitialMagazines;
        weapon->ReloadDuration = rebalancerWeapon.ReloadDuration;
        weapon->CoopSpareAmmoMultiplier = rebalancerWeapon.CoopSpareAmmoMultiplier;
        weapon->IdealRange = rebalancerWeapon.IdealRange;
        weapon->MaxRange = rebalancerWeapon.MaxRange;
        weapon->MaxTraceRange = rebalancerWeapon.MaxTraceRange;
        weapon->MaxRangeDamageMultiplier = rebalancerWeapon.MaxRangeDamageMultiplier;
        weapon->MeleeRange = rebalancerWeapon.MeleeRange;
        weapon->MeleeLungeRange = rebalancerWeapon.MeleeLungeRange;
        weapon->MeleeOuterSearchStepSize = rebalancerWeapon.MeleeOuterSearchStepSize;
        weapon->MeleeInnerSearchStepSize = rebalancerWeapon.MeleeInnerSearchStepSize;
        weapon->QuickMeleePct = rebalancerWeapon.QuickMeleePct;
        weapon->ZoomCurrentFOV = rebalancerWeapon.ZoomCurrentFOV;
        weapon->MaxAdhesionTime = rebalancerWeapon.MaxAdhesionTime;
        weapon->MinAdhesionDistance = rebalancerWeapon.MinAdhesionDistance;
        weapon->MinSnapAdhesionDistance = rebalancerWeapon.MinSnapAdhesionDistance;
        weapon->AimAssistScale = rebalancerWeapon.AimAssistScale;
        weapon->AimAssistScaleWhileZoomed = rebalancerWeapon.AimAssistScaleWhileZoomed;

        // Later
        //TArray< float > BaseSpread;
        //TArray< float > TABaseSpread;


        weapon->SpreadAccumulationRate = rebalancerWeapon.SpreadAccumulationRate;
        weapon->SpreadRecoveryRate = rebalancerWeapon.SpreadRecoveryRate;
        weapon->MaximumSpread = rebalancerWeapon.MaximumSpread;
        weapon->SpreadZoomMultiplier = rebalancerWeapon.SpreadZoomMultiplier;
        weapon->SpreadCrouchMultiplier = rebalancerWeapon.SpreadCrouchMultiplier;
        weapon->SpreadJumpMultiplier = rebalancerWeapon.SpreadJumpMultiplier;
        weapon->SpreadJumpConstant = rebalancerWeapon.SpreadJumpConstant;
        weapon->MovementSpreadConstant = rebalancerWeapon.MovementSpreadConstant;
        weapon->MovementSpreadMultiplier = rebalancerWeapon.MovementSpreadMultiplier;
        weapon->SpreadCenter = rebalancerWeapon.SpreadCenter;
        weapon->SpreadCenterWeight = rebalancerWeapon.SpreadCenterWeight;
        weapon->SpreadPerShot = rebalancerWeapon.SpreadPerShot;
        weapon->SpreadPerShotExp = rebalancerWeapon.SpreadPerShotExp;
        weapon->SpreadShotCount = rebalancerWeapon.SpreadShotCount;
        weapon->SpreadShotCountDecayRate = rebalancerWeapon.SpreadShotCountDecayRate;
        weapon->SpreadShotCountDecayExp = rebalancerWeapon.SpreadShotCountDecayExp;
        weapon->SpreadLastFireTime = rebalancerWeapon.SpreadLastFireTime;
        weapon->ModSpreadMultipler = rebalancerWeapon.ModSpreadMultipler;
        weapon->SpreadAccumulationModifier = rebalancerWeapon.SpreadAccumulationModifier;

        // Later
        //TArray< struct FVector2D > SpreadData;

        weapon->MaxOffsetMagnitude = rebalancerWeapon.MaxOffsetMagnitude;
        weapon->SpreadOffsetNormalWeight = rebalancerWeapon.SpreadOffsetNormalWeight;
        weapon->SpreadOffsetTightAimWeight = rebalancerWeapon.SpreadOffsetTightAimWeight;
        weapon->RecoilSizeModifier = rebalancerWeapon.RecoilSizeModifier;
        weapon->RecoilSize = rebalancerWeapon.RecoilSize;

        weapon->RecoilSizeVector.X = rebalancerWeapon.RecoilSizeVector.X;
        weapon->RecoilSizeVector.Y = rebalancerWeapon.RecoilSizeVector.Y;

        weapon->PCRecoilReductionMultiplier.X = rebalancerWeapon.PCRecoilReductionMultiplier.X;
        weapon->PCRecoilReductionMultiplier.Y = rebalancerWeapon.PCRecoilReductionMultiplier.Y;

        weapon->PCRecoilAccumulationReductionMultiplier = rebalancerWeapon.PCRecoilAccumulationReductionMultiplier;

        weapon->ConsoleRecoilReductionMultiplier.X = rebalancerWeapon.ConsoleRecoilReductionMultiplier.X;
        weapon->ConsoleRecoilReductionMultiplier.Y = rebalancerWeapon.ConsoleRecoilReductionMultiplier.Y;

        weapon->ConsoleRecoilAccumulationReductionMultiplier = rebalancerWeapon.ConsoleRecoilAccumulationReductionMultiplier;

        // Later
        //TArray< struct FVector2D > RecoilVectorOffset;

        weapon->MaxRecoilVector.X = rebalancerWeapon.MaxRecoilVector.X;
        weapon->MaxRecoilVector.Y = rebalancerWeapon.MaxRecoilVector.Y;

        weapon->MinRecoilVector.X = rebalancerWeapon.MinRecoilVector.X;
        weapon->MinRecoilVector.Y = rebalancerWeapon.MinRecoilVector.Y;

        weapon->MinRecoilMultipler = rebalancerWeapon.MinRecoilMultipler;
        weapon->RecoilAccumulation = rebalancerWeapon.RecoilAccumulation;
        weapon->RecoilZoomMultiplier = rebalancerWeapon.RecoilZoomMultiplier;
        weapon->RecoilRecoveryTime = rebalancerWeapon.RecoilRecoveryTime;
        weapon->RecoilApplyTime = rebalancerWeapon.RecoilApplyTime;
        weapon->WeaponReloadRate = rebalancerWeapon.WeaponReloadRate;
    }
}